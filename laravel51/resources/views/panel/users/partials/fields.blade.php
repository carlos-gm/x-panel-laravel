    <div class="form-body"> 
        <div class="form-group"> 
            {!! Form::label('name', 'Name:', ['class' => 'col-sm-3 col-md-3 col-lg-2 control-label']) !!} 
            <div class="col-md-4"> 
            {!! Form::text('name', null, ['class' => 'form-control input-sm']) !!} 
            </div> 
        </div> 

        <div class="form-group"> 
            {!! Form::label('email', 'Email:', ['class' => 'col-sm-3 col-md-3 col-lg-2 control-label']) !!} 
            <div class="col-md-4"> 
            {!! Form::text('email', null, ['class' => 'form-control input-sm']) !!} 
            </div> 
        </div> 

        <div class="form-group"> 
            {!! Form::label('password', 'Password:', ['class' => 'col-sm-3 col-md-3 col-lg-2 control-label']) !!} 
            <div class="col-md-4"> 
            {!! Form::text('password', '', ['class' => 'form-control input-sm', 'placeholder' => '**********']) !!} 
            </div> 
        </div> 

        <div class="form-group"> 
            {!! Form::label('group_id', 'Grupo:', ['class' => 'col-sm-3 col-md-3 col-lg-2 control-label']) !!} 
            <div class="col-md-4"> 
            {!! Form::select('group_id', $group_list , null, ['class' => 'form-control']) !!} 
            </div> 
        </div>

        <div class="form-group"> 
            {!! Form::label('active', 'Active:', ['class' => 'col-sm-3 col-md-3 col-lg-2 control-label']) !!} 
            <label class="checkbox-inline">
                {!! Form::checkbox('active', '1') !!} 
            </label>
        </div> 
    </div>
    