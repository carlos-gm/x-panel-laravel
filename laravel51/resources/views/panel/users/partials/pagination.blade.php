<div class="row">
	<div class="col-xs-4">
		<p class=" alert">
			<strong>{{ $users->total() }}</strong> Usuarios en un total de <strong>{{ $users->lastpage() }}</strong> páginas
		</p>
	</div>
	<div class="col-xs-8 text-right">
	{!!  $users->appends(Request::all())->render() !!}
	</div>
</div>