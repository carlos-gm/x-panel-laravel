@if(Session::get('message-success'))
	<div class="alert alert-success" id="usr-message">
		{{ date("H:i:s") . " - " . Session::get('message-success') }}
	</div>
@endif

@if(Session::get('message-warning'))
	<div class="alert alert-warning" id="usr-message">
		{{ date("H:i:s") . " - " . Session::get('message-warning') }}
	</div>
@endif

@if($errors->has())
	<div class="alert alert-danger">
		<ul>
    @foreach ($errors->all() as $error)
        <li>{{ date("H:i:s") . " - " . $error }}</li>
    @endforeach
    	</ul>
    </div>
@endif