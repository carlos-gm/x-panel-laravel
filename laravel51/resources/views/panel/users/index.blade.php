@extends('panel.layouts.general')

@section('breadcrumb')
<ul class="page-breadcrumb">
	<li>
		<i class="fa fa-home"></i>
		<a href="{{ config('panel.routes.home') }}">Inicio</a>
		<i class="fa fa-angle-right"></i>
	</li>
	<li>
		<a href="{{ config('panel.routes.users.index') }}">Configuración de usuarios</a>
	</li>
</ul>
@endsection

@section('content')
<div class="row">
	<div class="col-xs-12">
		@include('panel.users.partials.messages')

		@if($AccessControl->canCreate)
		<div class="text-right">
			<a href="{{ config('panel.routes.users.create') }}" class="btn btn-danger"><i class="fa fa-plus"></i> Crear usuario</a>
		</div>
		@endif

		<br />
		<div class="clearfix"></div>

		<!-- BEGIN EXAMPLE TABLE PORTLET-->
		<div class="portlet box blue">
			<div class="portlet-title">
				<div class="caption">
					<i class="fa fa-users"></i>Listado de usuarios
				</div>
			</div>
			
			<div class="portlet-body">
				<div class="widget-content nopadding">
					@include('panel.users.partials.search')
					<table class="table table-bordered table-striped table-hover">
						<thead>
							<tr>
								<th>Id</th>
								<th>Nombre</th>
								<th>E-mail</th>
								<th>Tipo</th>
								<th>Opciones</th>
							</tr>
						</thead>
						<tbody>
							@foreach ($users as $user)
							<tr>
								<td>{{ $user->id }}</td>
								<td>{{ $user->name }}</td>
								<td>{{ $user->email }}</td>
								<td class="text-center col-xs-2">
									@if($user->group_id == '4')
									    <span class="label label-sm label-danger">Administrador</span>
									@else
										<span class="in-progress">No establecido: grupo {{ $user->group_id }}</span>
									@endif
								</td>
								<td class="text-center col-xs-2">
									@if($AccessControl->canDelete)
									<a href="{{ config('panel.routes.users.delete') }}{{ $user->id }}" class="btn btn-danger btn-xs delete">
										<i class="fa fa-remove"></i>
										Borrar</a>
									@endif
									@if($AccessControl->canEdit)
									<a href="{{ config('panel.routes.users.edit') }}{{ $user->id }}" class="btn btn-success btn-xs">
										<i class="fa fa-edit"></i>
										Editar</a>
									@endif
								</td>
							</tr>
							@endforeach
						</tbody>
					</table>
					@include('panel.users.partials.pagination')
				</div><!--portlet-body-->
				<!-- END EXAMPLE TABLE PORTLET-->
			</div>
		</div>
	</div>
</div>
@stop

@section('scripts')

/*
$('#usr-message').pulsate({
    color: "#bc0e0e"
});
*/

$('.delete').confirmModal({
    confirmTitle     : '¡Atención! Esta acción no se puede deshacer.',
    confirmMessage   : '¿Seguro que desea borrar el usuario?',
    confirmOk        : 'Borrar',
    confirmCancel    : 'Cancelar',
    confirmDirection : 'rtl',
    confirmStyle     : 'danger',
    confirmDismiss   : true,
});
@stop