@extends('panel.layouts.general')

@section('breadcrumb')
<ul class="page-breadcrumb">
    <li>
        <i class="fa fa-home"></i>
        <a href="{{ config('panel.routes.home') }}">Inicio</a>
        <i class="fa fa-angle-right"></i>
    </li>
    <li>
        <a href="{{ config('panel.routes.users.index') }}">Configuración de usuarios</a>
        <i class="fa fa-angle-right"></i>
    </li>
    <li>
        <a href="javascript: return false;">Crear usuario</a>
    </li>
</ul>
@endsection

@section('content')
@include('panel.users.partials.messages')

        <div class="text-right">
            <a href="{{ config('panel.routes.users.index') }}" class="btn red"><i class="fa fa-arrow-left"></i> Regresar al listado</a>
        </div>
        
        <br />
        <div class="clearfix"></div>

<!-- BEGIN SAMPLE FORM PORTLET-->
    @if($errors->any())
        <div class="alert alert-danger">
        @foreach($errors->all() as $error)
            <button class="close" data-close="alert"></button>
            <span>{{ $error }}</span>
        @endforeach
        </div>
    @endif
        <div class="portlet box red "> 
            <div class="portlet-title"> 
                <div class="caption"> 
                    <i class="fa fa-user"></i> Crear Usuario
                </div> 
            </div> 
        <div class="portlet-body form"> 
        {!! Form::model(Request::all(), ['url' => config('panel.routes.users.store'), 'method' => 'POST', 'class' => 'form-horizontal', 'role' => 'form']) !!} 
            @include('panel.users.partials.fields')
            <div class="form-actions text-right"> 
                <div class="row"> 
                    <div class="col-md-offset-3 col-md-9"> 
                        {!! Form::submit('Guardar', [ 'class' => 'btn btn-danger']) !!} 
                    </div> 
                </div> 
            </div> 
        {!! Form::close() !!} 
        </div> 
    </div> 
<!-- END SAMPLE FORM PORTLET-->
@stop