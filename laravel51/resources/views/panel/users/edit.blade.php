@extends('panel.layouts.general')

@section('content')

@section('breadcrumb')
<ul class="page-breadcrumb">
    <li>
        <i class="fa fa-home"></i>
        <a href="{{ config('panel.routes.home') }}">Inicio</a>
        <i class="fa fa-angle-right"></i>
    </li>
    <li>
        <a href="{{ config('panel.routes.users.index') }}">Configuración de usuarios</a>
        <i class="fa fa-angle-right"></i>
    </li>
    <li>
        <a href="javascript: return false;">Actualiza el usuario</a>
    </li>
</ul>
@endsection

@include('panel.users.partials.messages')
        <div class="text-right">
            <a href="{{ config('panel.routes.users.index') }}" class="btn btn-success"><i class="fa fa-arrow-left"></i> Regresar al listado</a>
        </div>
        
        <br />
        <div class="clearfix"></div>

<!-- BEGIN SAMPLE FORM PORTLET--> 
        <div class="portlet box green "> 
            <div class="portlet-title"> 
                <div class="caption"> 
                    <i class="fa fa-user"></i> Actualizar Usuario
                </div> 
            </div> 
        <div class="portlet-body form"> 

        {!! Form::model($user, ['url' => [config('panel.routes.users.update'), $user->id], 'method' => 'POST', 'class' => 'form-horizontal', 'role' => 'form']) !!} 
            @include('panel.users.partials.fields')
            <div class="form-actions text-right"> 
                <div class="row"> 
                    <div class="col-md-offset-3 col-md-9"> 
                        {!! Form::submit('Actualizar', [ 'class' => 'btn btn-success']) !!} 
                    </div> 
                </div> 
            </div> 
        {!! Form::close() !!} 
        </div> 
<!-- END SAMPLE FORM PORTLET-->
@stop