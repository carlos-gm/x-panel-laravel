    <div class="form-body"> 

        <div class="form-group"> 
            {!! Form::label('name', 'Nombre:', ['class' => 'col-sm-3 col-md-3 col-lg-2 control-label']) !!} 
            <div class="col-md-4"> 
            {!! Form::text('name', null, ['class' => 'form-control input-sm']) !!} 
            </div> 
        </div> 

        <div class="form-group"> 
            {!! Form::label('lang', 'Idioma:', ['class' => 'col-sm-3 col-md-3 col-lg-2 control-label']) !!} 
            <div class="col-xs-2"> 
            {!! Form::select('lang', config('panel.options.langs'), null, ['class' => 'form-control']) !!} 
            </div> 
        </div> 

        <div class="form-group"> 
            {!! Form::label('title', 'Título:', ['class' => 'col-sm-3 col-md-3 col-lg-2 control-label']) !!} 
            <div class="col-md-4"> 
            {!! Form::text('title', null, ['class' => 'form-control input-sm']) !!} 
            </div> 
        </div> 

        <div class="form-group"> 
            {!! Form::label('url', 'Url:', ['class' => 'col-sm-3 col-md-3 col-lg-2 control-label']) !!} 
            <div class="col-md-4"> 
            {!! Form::text('url', null, ['class' => 'form-control input-sm']) !!} 
            <label class="label label-sm label-default" style="float: right; display: block;">.html</label>
            </div> 

        </div> 

        <div class="form-group"> 
            {!! Form::label('keywords', 'Keywords:', ['class' => 'col-sm-3 col-md-3 col-lg-2 control-label']) !!} 
            <div class="col-md-4"> 
            {!! Form::text('keywords', null, ['class' => 'form-control input-sm']) !!} 
            </div> 
        </div> 

        <div class="form-group"> 
            {!! Form::label('description', 'Description:', ['class' => 'col-sm-3 col-md-3 col-lg-2 control-label']) !!} 
            <div class="col-md-4"> 
            {!! Form::text('description', null, ['class' => 'form-control input-sm']) !!} 
            </div> 
        </div> 

        <div class="form-group"> 
            {!! Form::label('template', 'Plantilla:', ['class' => 'col-sm-3 col-md-3 col-lg-2 control-label']) !!} 
            <div class="col-md-4"> 
            {!! Form::select('template', config('panel.pages.templates'), null, ['class' => 'form-control']) !!} 
            </div> 
        </div> 
        
        <div class="form-group"> 
            {!! Form::label('active', 'Active:', ['class' => 'col-sm-3 col-md-3 col-lg-2 control-label']) !!} 
            <div class="checkbox-inline"> 
            {!! Form::checkbox('active', '1', null) !!} 
            </div> 
        </div> 

        <div class="form-group"> 
            {!! Form::label('content', 'Content:', ['class' => 'col-sm-3 col-md-3 col-lg-2 control-label']) !!} 
            <div class="col-md-9"> 
            {!! Form::textarea('content', null, ['class' => 'form-control']) !!}
            </div> 
        </div> 

        @include('panel.pages.partials.tips')
        
    </div>
    