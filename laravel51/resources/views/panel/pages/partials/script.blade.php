$("#title").stringToSlug({
    setEvents: 'keyup keydown blur',
    getPut: '#url',
    space: '-',
    prefix: '',
    suffix: '',
    replace: '',
    AND: 'and',
    callback: false
});

var config_full = {
    skin: 'moono-dark',
    height: '300px',
    //baseUrl: "/data/upload/",
    fontSize_defaultLabel : "14px",
    filebrowserImageBrowseUrl : '/assets/global/plugins/ckeditor/plugins/ckfinder/ckfinder.html?type=Images',
    filebrowserImageUploadUrl : '/assets/global/plugins/ckeditor/plugins/ckfinder/upload.php?type=images',
    extraPlugins: 'autogrow,codemirror,wenzgmap',
    autoGrow_onStartup: true,
    /*autoGrow_maxHeight: 1000,*/
    toolbar:
    [
        [ 'Source', '-', 'NewPage', 'Preview', '-', 'Templates' ],
        [ 'Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord', '-', 'Undo', 'Redo' ],
        [ 'Find', 'Replace', '-', 'SelectAll', '-', 'Scayt' ],
        [ 'Bold', 'Italic', 'Underline', 'Strike', '-', 'RemoveFormat' ],
        '/',
        [ 'NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', '-', 'Blockquote', 'CreateDiv', '-', 'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock' ],
        [ 'Image', 'Flash', 'wenzgmap', 'Table', 'HorizontalRule', 'PageBreak', 'Iframe' ],
        [ 'Link', 'Unlink' ],
        '/',
        [ 'Styles', 'Format', 'Font', 'FontSize', 'Small' ],
        [ 'TextColor', 'BGColor' ],
        [ 'Maximize', 'ShowBlocks' ],
        [ 'About' ],
    ],
    stylesSet : [
    { name:'Label Default', element:'span', attributes:{ 'class':'label' } },
    { name:'Label Success', element:'span', attributes:{ 'class':'label label-success' } },
    { name:'Label Warning', element:'span', attributes:{ 'class':'label label-warning' } },
    { name:'Label Info', element:'span', attributes:{ 'class':'label label-info' } },
    { name:'Well Default', element:'div', attributes:{ 'class':'well' } },
    { name:'Well White', element:'div', attributes:{ 'class':'well well-white' } },
    { name:'Alert Success', element:'div', attributes:{ 'class':'alert alert-success' } },
    { name:'Alert Information', element:'div', attributes:{ 'class':'alert alert-info' } },
    { name:'Code', element:'code' },
    { name:'Pre', element:'pre', attributes:{'class':'prettyprint linenums'} },
    ],
    contentsCss : [ 'https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css',
                    '//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css'],
    bodyClass : 'container',
    allowedContent: true,
};

$('#content').ckeditor(config_full);

$("#template").change(function(){
    
    if(confirm("¿ Concatenar plantilla ?")){
        $.get( "{{ config('panel.routes.pages.load_tpl') }}", { template: $(this).val() }, function(data){
            $( '#content' ).val( $( '#content' ).val() + data );
        });
    }else{
        $.get( "{{ config('panel.routes.pages.load_tpl') }}", { template: $(this).val() }, function(data){
            $( '#content' ).val( data );
        });
    }
});