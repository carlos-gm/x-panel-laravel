<nav class="navbar pull-right">
      	{!! Form::open(['url' => config('panel.routes.pages.index'), 'method' => 'GET', 'class' => 'navbar-form navbar-right', 'role' => 'form']) !!} 
        <div class="form-group">
        	{!! Form::select('active', config('panel.options.active'), Input::get('active'), ['class' => 'form-control']) !!} 
        	{!! Form::select('lang', config('panel.options.langs'), Input::get('lang'), ['class' => 'form-control']) !!} 
        	{!! Form::text('search', Input::get('search'), ['class' => 'form-control', 'style' => 'width: 300px', 'placeholder' => 'Inserta un texto']) !!} 
        </div>
        {!! Form::submit('Buscar', [ 'class' => 'btn btn-primary']) !!} 
      {!! Form::close() !!} 

</nav>