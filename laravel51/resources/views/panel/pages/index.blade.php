@extends('panel.layouts.general')

@section('breadcrumb')
<ul class="page-breadcrumb">
	<li>
		<i class="fa fa-home"></i>
		<a href="{{ config('panel.routes.home') }}">Inicio</a>
		<i class="fa fa-angle-right"></i>
	</li>
	<li>
		<a href="{{ config('panel.routes.main.index') }}">Pantalla principal</a>
	</li>
</ul>
@endsection

@section('content')
<div class="row">
	<div class="col-xs-12">
		@include('panel.users.partials.messages')

		@if($AccessControl->canCreate)
		<div class="text-right">
			<a href="{{ config('panel.routes.pages.create') }}" class="btn btn-danger"><i class="fa fa-plus"></i> @lang('pages.index.crate_page')</a>
		</div>
		@endif

		<br />
		<div class="clearfix"></div>

		<!-- BEGIN EXAMPLE TABLE PORTLET-->
		<div class="portlet box blue">
			<div class="portlet-title">
				<div class="caption">
					<i class="fa fa-users"></i>@lang('pages.index.list_pages')
				</div>
			</div>
			
			<div class="portlet-body">
				<div class="widget-content nopadding">
					@include('panel.pages.partials.search')
					<table class="table table-bordered table-striped table-hover">
						<thead>
							<tr>
								<th>Id</th>
								<th>@lang('pages.index.name')</th>
								<th>@lang('pages.index.url')</th>
								<th>@lang('pages.index.lang')</th>
								<th>@lang('pages.index.status')</th>
								<th>@lang('pages.index.options')</th>
							</tr>
						</thead>
						<tbody>
							@foreach($pages as $page)
							<tr>
								<td>{{ $page->id }}</td>
								<td>{{ $page->name }}</td>
								<td>{{ $page->url }}</td>
								<td class="text-center col-xs-1"><img src="{{ URL::asset('assets/global/img/flags') }}/{{ $page->lang }}.png" /></td>
								<td class="text-center col-xs-1">
								@if($page->active == 1)
									<span class="label label-sm label-success">Activo</span>
								@else
									<span class="label label-sm label-default">Inactivo</span>
								@endif
								</td>
								<td class="text-center col-xs-2">
									@if($AccessControl->canDelete)
									<a href="{{ config('panel.routes.pages.delete') }}{{ $page->id }}" class="btn btn-danger btn-xs delete">
										<i class="fa fa-remove"></i>
										Borrar</a>
									@endif
									@if($AccessControl->canEdit)
									<a href="{{ config('panel.routes.pages.edit') }}{{ $page->id }}" class="btn btn-success btn-xs">
										<i class="fa fa-edit"></i>
										Editar</a>
									@endif
								</td>
							</tr>
							@endforeach
						</tbody>
					</table>
				</div><!--portlet-body-->
				<!-- END EXAMPLE TABLE PORTLET-->
			</div>
		</div>
	</div>
</div>
@stop

@section('scripts')

$('.delete').confirmModal({
    confirmTitle     : '¡Atención! Esta acción no se puede deshacer.',
    confirmMessage   : '¿Seguro que desea borrar el usuario?',
    confirmOk        : 'Borrar',
    confirmCancel    : 'Cancelar',
    confirmDirection : 'rtl',
    confirmStyle     : 'danger',
    confirmDismiss   : true,
});
@stop