@extends('panel.layouts.general')

@section('content')

@section('breadcrumb')
<ul class="page-breadcrumb">
    <li>
        <i class="fa fa-home"></i>
        <a href="{{ config('panel.routes.home') }}">Inicio</a>
        <i class="fa fa-angle-right"></i>
    </li>
    <li>
        <a href="{{ config('panel.routes.pages.index') }}">Configuración de usuarios</a>
        <i class="fa fa-angle-right"></i>
    </li>
    <li>
        <a href="javascript: return false;">@lang("pages.index.update_page")</a>
    </li>
</ul>
@endsection

@include('panel.pages.partials.messages')
        <div class="text-right">
            <a href="{{ config('panel.routes.pages.index') }}" class="btn btn-success"><i class="fa fa-arrow-left"></i> Regresar al listado</a>
        </div>
        <br />
        <div class="clearfix"></div>

<!-- BEGIN SAMPLE FORM PORTLET--> 
        <div class="portlet box green "> 
            <div class="portlet-title"> 
                <div class="caption"> 
                    <i class="fa fa-page"></i> Actualizar Usuario
                </div> 
            </div> 

        <div class="portlet-body form"> 
        {!! Form::model($page, ['url' => [config('panel.routes.pages.update'), $page->id], 'method' => 'POST', 'class' => 'form-horizontal', 'role' => 'form']) !!} 
            @include('panel.pages.partials.fields')
            <div class="form-actions text-right"> 
                <div class="row"> 
                    <div class="col-md-offset-3 col-md-9"> 
                        {!! Form::submit('Actualizar', [ 'class' => 'btn btn-success']) !!} 
                    </div> 
                </div> 
            </div> 
        {!! Form::close() !!} 
        </div> 
<!-- END SAMPLE FORM PORTLET-->
@stop

@section('load_scripts')
<script src="/assets/global/plugins/ckeditor/ckeditor.js"></script>
<script src="http://ckeditor.com/ckeditor_4.3_beta/adapters/jquery.js"></script>
<script src="/assets/admin/pages/scripts/jquery.stringToSlug.min.js"></script>

@stop

@section('scripts')

@include('panel.pages.partials.script')

@stop