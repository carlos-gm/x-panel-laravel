@extends('panel.layouts.general')

@section('breadcrumb')
<ul class="page-breadcrumb">
	<li>
		<i class="fa fa-home"></i>
		<a href="{{ config('panel.routes.home') }}">Inicio</a>
		<i class="fa fa-angle-right"></i>
	</li>
	<li>
		<a href="{{ config('panel.routes.groups.index') }}">Configuración de grupos de usuario</a>
	</li>
</ul>
@endsection

@section('content')
<div class="row">
	<div class="col-xs-12">
		@include('panel.groups.partials.messages')

		@if($AccessControl->canCreate)
		<div class="text-right">
			<a href="{{ config('panel.routes.groups.create') }}" class="btn btn-danger"><i class="fa fa-plus"></i> Crear grupo</a>
		</div>
		@endif

		<br />
		<div class="clearfix"></div>

		<!-- BEGIN EXAMPLE TABLE PORTLET-->
		<div class="portlet box blue">
			<div class="portlet-title">
				<div class="caption">
					<i class="fa fa-users"></i>Lista de grupos
				</div>
			</div>
			
			<div class="portlet-body">
				<div class="widget-content nopadding">
					<table class="table table-bordered table-striped table-hover">
						<thead>
							<tr>
								<th>Grupo</th>
								<th>Opciones</th>
							</tr>
						</thead>
						<tbody>
							@foreach ($groups as $group)
							<tr>
								<td>{{ $group->name }}</td>
								<td class="text-center">
									@if($AccessControl->canEdit)
									<a href="{{ config('panel.routes.groups.edit') }}{{ $group->id }}" class="btn btn-success btn-xs">
										<i class="fa fa-edit"></i>
										Editar</a>
									@endif
								</td>
							</tr>
							@endforeach
						</tbody>
					</table>
				</div><!--portlet-body-->
				<!-- END EXAMPLE TABLE PORTLET-->
			</div>
		</div>
	</div>
</div>
@stop

@section('scripts')

$('#usr-message').pulsate({
    color: "#bc0e0e"
});

$('.delete').confirmModal({
    confirmTitle     : '¡Atención! Esta acción no se puede deshacer.',
    confirmMessage   : '¿Seguro que desea borrar el usuario?',
    confirmOk        : 'Borrar',
    confirmCancel    : 'Cancelar',
    confirmDirection : 'rtl',
    confirmStyle     : 'danger',
    confirmDismiss   : true,
});
@stop