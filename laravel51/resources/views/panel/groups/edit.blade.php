@extends('panel.layouts.general')

@section('content')

@section('breadcrumb')
<ul class="page-breadcrumb">
    <li>
        <i class="fa fa-home"></i>
        <a href="{{ config('panel.routes.home') }}">Inicio</a>
        <i class="fa fa-angle-right"></i>
    </li>
    <li>
        <a href="{{ config('panel.routes.groups.index') }}">Configuración de grupos de usuario</a>
    </li>
</ul>
@endsection

@include('panel.groups.partials.messages')

        <div class="text-right">
            <a href="{{ config('panel.routes.groups.index') }}" class="btn btn-success"><i class="fa fa-arrow-left"></i> Regresar al listado</a>
        </div>
        
        <br />
        <div class="clearfix"></div>

<!-- BEGIN SAMPLE FORM PORTLET--> 
        <div class="portlet box green "> 
            <div class="portlet-title"> 
                <div class="caption"> 
                    <i class="fa fa-user"></i> Actualizar Grupo de Usuarios
                </div> 
            </div> 
        <div class="portlet-body form">
            {!! Form::model( $group, ['url' => [config('panel.routes.groups.update'), $group->id], 'method' => 'POST', 'class' => 'form-horizontal', 'role' => 'form']) !!}
            @include('panel.groups.partials.fields')
            @if($AccessControl->canEdit)
            <div class="form-actions text-right"> 
                <div class="row"> 
                    <div class="col-md-offset-3 col-md-9"> 
                        {!! Form::submit('Actualizar', [ 'class' => 'btn btn-success']) !!} 
                    </div> 
                </div> 
            </div>
            @endif
            {!! Form::close() !!} 
        </div> 
<!-- END SAMPLE FORM PORTLET-->
@stop