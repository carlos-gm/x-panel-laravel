@if(Session::get('message'))
	<div class="alert alert-success" id="usr-message">
		{{ date("H:i:s") . " - " . Session::get('message') }}
	</div>
@endif

@if($errors->has())
	<div class="alert alert-danger">
		<ul>
    @foreach ($errors->all() as $error)
        <li>{{ date("H:i:s") . " - " . $error }}</li>
    @endforeach
    	</ul>
    </div>
@endif