    <div class="form-body">

           <div class="form-group"> 
            {!! Form::label('name', 'Nombre:', ['class' => 'autosizeme col-sm-3 col-md-3 col-lg-2 control-label']) !!} 
            <div class="col-md-4">
                <div>
                    <div class="input-group">
                        <span class="input-group-addon">
                        <i class="fa fa-user"></i>
                        </span>
                        {!! Form::text('name', null, ['class' => 'form-control input-sm']) !!} 
                    </div>
                </div>
            </div> 
        </div> 
        <div class="clearfix"></div>

        <div class="form-group"> 
            {!! Form::label('description', 'Descripción:', ['class' => 'col-sm-3 col-md-3 col-lg-2 control-label']) !!} 
            <div class="col-md-4"> 
                {!! Form::text('description', null, ['class' => 'form-control input-sm']) !!} 
            </div> 
        </div> 
        <div class="clearfix"></div>

        <div class="form-group"> 
            {!! Form::label('active', 'Activo:', ['class' => 'col-sm-3 col-md-3 col-lg-2 control-label']) !!} 
            <label class="checkbox-inline">
                {!! Form::checkbox('active', '1') !!} 
            </label> 
        </div> 
        <div class="clearfix"></div>

        <div class="form-group"> 
            {!! Form::label('xxx', 'Permisos:', ['class' => 'col-sm-3 col-md-3 col-lg-2 control-label']) !!} 
        </div> 

        <?php foreach($permissions as $pkey => $permission):?>
        <div class="note note-info">
        <div class="form-group"> 
            <label for="<?=$pkey?>" class="col-sm-3 col-md-3 col-lg-2 control-label"><strong><?=$permission["name"] ?>:</strong></label>
        </div>

        <div class="form-group">
                <div class="clearfix"></div>
                <?php $apps = $permission["apps"]; ?>
                <?php foreach($apps as $akey => $app):?>

                <!--<div class="form-group col-md-4 col-md-push-1">-->
                <div class="form-group" style="padding-left: 150px">
                    <label class="col-sm-3 col-md-3 col-lg-2 control-label"><u><?=$app["name"] ?>:</u></label>
                    <div class="checkbox-list">
                    <?php $configApp = $app["actions"]; ?>

                    <?php foreach($configApp as $ckey => $app):?>
                        <?php if(isset($group->permissions[$pkey]->$akey->$app)): ?>
                        <label class="checkbox-inline">
                            <input name="permissions[<?=$pkey?>][<?=$akey?>][<?=$app?>]" <?=($group->permissions[$pkey]->$akey->$app)?"checked":"" ?> value="1"  type="checkbox">
                            <?=ucfirst($app) ?>
                        </label>
                        <?php else: ?>
                        <label class="checkbox-inline">
                            <input name="permissions[<?=$pkey?>][<?=$akey?>][<?=$app?>]" value="1"  type="checkbox"><?=ucfirst($app) ?></label>
                        <?php endif; ?>
                    <?php endforeach; ?>
                    </div><!--checkbox-inline-->
                </div>
                <?php endforeach; ?>
        </div><!--form group-->
        </div>
        <?php endforeach; ?>
    </div><!--form body-->
