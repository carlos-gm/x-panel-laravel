<?php 


 return array(
 	'path'		=> '../public/data/templates/',
 	'templates' => [
 		''				=> 'Carga una plantilla',
 		'info.tpl.html'				=> 'Info',
 		'landing_v1.tpl.html'		=> 'Landing #1',
 		'landing_v2.tpl.html'		=> 'Landing #2',
 		'landing_v3.tpl.html'		=> 'Landing #3',
 		'localization.tpl.html' 	=> 'Localizacion', 
 		'colums.tpl.html'			=> 'Colums #1', 
 		'contact.tpl.html'			=> 'Contact #1', 
		'contact_v2.tpl.html'		=> 'Contact #2', 
		'contact_v3.tpl.html'		=> 'Contact #3', 
 		'about_us.tpl.html'			=> 'About Us', 
 		'gallery.tpl.html'			=> 'Gallery #1', 
 		'form_landing.tpl.html'		=> 'Landing #1', 
 		'team.tpl.html'				=> 'Team #1',
 		'pricing.tpl.html'			=> 'Pricing #1', 
 		'link_gallery.tpl.html'		=> 'Link Gallery'
 		]
 );