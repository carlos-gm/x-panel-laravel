<?php 

  return array(

 	'config' => [
 		'users' => [
 			'name' => 'Usuario',
 			'icon' => 'fa-user'
 		],
 		'groups' => [
 			'name' => 'Grupos',
 			'icon' => 'fa-group'
 		],
 		'general' => [
 			'name' => 'General',
 			'icon' => 'fa-gears'
 		],
 		'system' => [
 			'name' => 'Sistema',
 			'icon' => 'fa-desktop'
 		],
 		'system' => [
 			'name' => 'Sistema',
 			'icon' => 'fa-desktop'
 		],
 		'snipets' => [
 			'name' => 'Code Snipets',
 			'icon' => 'fa-desktop'
 		],
	]
 );
