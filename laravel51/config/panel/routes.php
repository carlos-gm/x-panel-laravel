<?php 
 
 $prefixRoot 	= "/panel";

 $prefixUsers 	= "/users";
 $prefixGroups 	= "/groups";
 $prefixPages 	= "/pages";

 return array(
 	'home' 	=> $prefixRoot,
 	'users' => [
 		'index' 	=> $prefixRoot . $prefixUsers,
 		'store' 	=> $prefixRoot . $prefixUsers . '/store',
 		'update' 	=> $prefixRoot . $prefixUsers . '/update',
 		'create' 	=> $prefixRoot . $prefixUsers . '/create/',
 		'edit' 		=> $prefixRoot . $prefixUsers . '/edit/',
 		'delete'	=> $prefixRoot . $prefixUsers . '/delete/',
 	],
 	'groups' => [
 		'index' 	=> $prefixRoot . $prefixGroups,
 		'store' 	=> $prefixRoot . $prefixGroups . '/store',
 		'update' 	=> $prefixRoot . $prefixGroups . '/update',
 		'create' 	=> $prefixRoot . $prefixGroups . '/create/',
 		'edit' 		=> $prefixRoot . $prefixGroups . '/edit/',
 		'delete'	=> $prefixRoot . $prefixGroups . '/delete/',
 	],
 	'pages' => [
 		'index' 	=> $prefixRoot . $prefixPages,
 		'store' 	=> $prefixRoot . $prefixPages . '/store',
 		'update' 	=> $prefixRoot . $prefixPages . '/update',
 		'create' 	=> $prefixRoot . $prefixPages . '/create/',
 		'edit' 		=> $prefixRoot . $prefixPages . '/edit/',
 		'delete'	=> $prefixRoot . $prefixPages . '/delete/',
 		'load_tpl' 	=> $prefixRoot . $prefixPages . '/load_tpl/',
 	],
 );