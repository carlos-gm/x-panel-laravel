<?php 

/*
https://cartalyst.com/manual/sentry/2.1 (IDEAS)
*/

/*	
	Permisos totales para un super admin
	{"dashboard":{"main":{"view":true}},"config":{"users":{"view":true,"edit":true,"create":true,"update":true,"delete":true,"activate":true},"groups":{"view":true,"edit":true,"create":true,"update":true,"activate":true},"snipets":{"view":true}}}
*/
/*
	Notas: El editor de páginas mediante tags tipo {{newlist@id}} o {{gallery#team}} (por id's o tags name) podrán cargar contenidos generados en las demas secciones (noticias, galerías, etc)
*/
 return array(
 	/***/
 	'dashboard' => [
 		'name' 			=> "Dashboard",
 		'icon'			=> "icon-home",
 		'description' 	=> "Grupo de apps destinada visualizar elementos generales de otras apps, estadisticas, etc..",
 		'active' 		=> ['deny','allow'],
 		'apps' 			=> [
					 		'main' => [
					 			'name' 			=> "Patalla Principal",
					 			'icon'			=> "icon-bar-chart",
					 			'description' 	=> "Pantalla que muestra estadisticas generales del panel de control",
					 			'route'			=> 'panel',
								'actions' 		=> ['view'],
					 		],
		],
 	],
 	/**/
 	//http://sdk.ckeditor.com/samples/inline.html
 	'pages' => [
 		'name' 			=> "Páginas",
 		'icon'			=> "fa fa-file-o",
 		'description' 	=> "Grupo de apps destinada visualizar elementos generales de otras apps, estadisticas, etc..",
 		'active' 		=> ['deny','allow'],
 		'apps' 			=> [
					 		'editor' => [
					 			'name' 			=> "Editor de páginas",
					 			'icon'			=> "fa fa-file-code-o",
					 			'description' 	=> "Pantalla que muestra las páginas ",
					 			'route'			=> 'panel.pages',
					 			'actions' 		=> ['view', 'edit', 'create','delete','activate'],
					 		],
		],
 	],
 	'news' => [
 		'name' 			=> "Noticias",
 		'icon'			=> "fa fa-newspaper-o",
 		'description' 	=> "Grupo de apps destinado a generar noticias por categorias",
 		'active' 		=> ['deny','allow'],
 		'apps' 			=> [
					 		'editor' => [
					 			'name' 			=> "Editor de noticias",
					 			'icon'			=> "fa fa-file-word-o",
					 			'description' 	=> "Pantalla que muestra las categorias del blog y sus noticias ",
					 			'route'			=> 'panel.blog',
					 			'actions' 		=> ['view', 'edit', 'create','delete','activate'],
					 		],
					 		'category' => [
					 			'name' 			=> "Categorias",
					 			'icon'			=> "fa fa-sitemap",
					 			'description' 	=> "Pantalla que muestra las categorias del blog y sus noticias ",
					 			'route'			=> 'panel.blog',
					 			'actions' 		=> ['view', 'edit', 'create','delete','activate'],
					 		],
		],
 	],
 	'gallery' => [
 		'name' 			=> "Galerías",
 		'icon'			=> "fa fa-file-photo-o",
 		'description' 	=> "Grupo de apps destinado a generar galerias de imágenes",
 		'active' 		=> ['deny','allow'],
 		'apps' 			=> [
					 		'editor' => [
					 			'name' 			=> "Listado",
					 			'icon'			=> "fa fa-file-word-o",
					 			'description' 	=> "Pantalla que muestra las categorias del blog y sus noticias ",
					 			'route'			=> 'panel.blog',
					 			'actions' 		=> ['view', 'edit', 'create','delete','activate'],
					 		],
					 		'category' => [
					 			'name' 			=> "Categorias",
					 			'icon'			=> "fa fa-sitemap",
					 			'description' 	=> "Pantalla que muestra las categorias del blog y sus noticias ",
					 			'route'			=> 'panel.blog',
					 			'actions' 		=> ['view', 'edit', 'create','delete','activate'],
					 		],
		],
 	],
 	'config' => [
 		'name' 			=> "Configuración",
 		'icon'			=> "icon-settings",
 		'description' 	=> "Grupo de apps destinada a gestionar el panel de control",
 		'active' 		=> ['deny','allow'],
 		'apps' 			=> [
					 		'users' => [
					 			'name' 			=> "Usuarios",
					 			'icon'			=> "fa fa-user",
					 			'description' 	=> "App encargada de gestionar los usuarios del panel de control",
					 			'route'			=> 'panel.users',
					 			'actions' 		=> ['view', 'edit', 'create','delete','activate'],
					 		],
							'groups' => [
					 			'name' 			=> "Grupos",
					 			'icon'			=> "fa fa-group",
					 			'description' 	=> "App encargada de los permisos de los usuarios del panel de control",
					 			'route'			=> 'panel.groups',
					 			'actions' 		=> ['view', 'edit', 'create','delete','activate'],
					 		],
							'menu' => [
					 			'name' 			=> "Menu Builder",
					 			'icon'			=> "fa fa-cog",
					 			'description' 	=> "App encargada de generar menús con https://styde.net/creando-menus-con-el-componente-stydehtml/ ",
					 			'route'			=> 'panel.snipets',
					 			'actions' 		=> ['view'],
					 		],
							'general' => [
					 			'name' 			=> "General",
					 			'icon'			=> "fa fa-cog",
					 			'description' 	=> "App encargada de controlar una tabla de la bbdd con paramentros de configuracion como default title, config de paginacion, google maps, etc",
					 			'route'			=> 'panel.snipets',
					 			'actions' 		=> ['view'],
					 		],
		],
 	],

 );