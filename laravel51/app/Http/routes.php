<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

/*
Route::get('/', function () {
    return view('welcome');
});
*/

//Route::get('/panel', 'Panel\IndexController@index');
/*
Route::get('/panel', function(){

	$section_name = "Inicio";
	
	$params = compact('section_name');

	return view('panel.index', $params);
});
*/

/*
Route::get('login', [
		'auth' => 'Auth\AuthController',
		'password' => 'Auth\PasswordController'
		]);
*/

/*
Event::listen('illuminate.query',function($query){
    file_put_contents("query_log.txt", $query . "\r\n", FILE_APPEND);
    file_put_contents("query_log.txt", var_export($_REQUEST, TRUE) . "\r\n", FILE_APPEND);
});
*/

//Auth::loginUsingId(1); //para forzar estar loguegados


Route::get('panel/login', 'Auth\AuthController@getLogin');
Route::post('panel/login', 'Auth\AuthController@postLogin');
Route::get('panel/logout', 'Auth\AuthController@getLogout');

Route::group(['prefix' => 'panel', 'middleware' => 'auth', 'namespace' => 'Panel'], function(){

	//Route::resource('users', 'UsersController', ['as' => 'users']);

	Route::get('/', 'DashboardController@index');

	Route::get('users/', 'UsersController@index');
	Route::get('users/edit/{id}', 'UsersController@edit');
	Route::get('users/create', 'UsersController@create');
	Route::post('users/store', 'UsersController@store');
	Route::post('users/update/{id}', 'UsersController@update');	
	Route::get('users/delete/{id}', 'UsersController@destroy');

	Route::get('groups/', 'GroupsController@index');
	Route::get('groups/edit/{id}', 'GroupsController@edit');
	Route::post('groups/update/{id}', 'GroupsController@update');
	Route::get('groups/create', 'GroupsController@create');
	Route::post('groups/store', 'GroupsController@store');

	Route::get('pages/', 'PagesController@index');
	Route::get('pages/edit/{id}', 'PagesController@edit');
	Route::post('pages/update/{id}', 'PagesController@update');
	Route::get('pages/create', 'PagesController@create');
	Route::post('pages/store', 'PagesController@store');
	Route::get('pages/load_tpl', 'PagesController@load_tpl');
	Route::get('pages/delete/{id}', 'PagesController@destroy');

});