<?php

namespace App\Http\Controllers\Panel;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use Auth;
use App\Panel\Menu;
use App\Panel\AccessControl;
use App\Panel\PageMaker;


//revisar esto http://cristianszwarc.com.ar/ckeditor/

class PagesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    
    public $section_name = "Editor de páginas";
    
    public function __construct(){
        $this->AccessControl = new AccessControl(Auth::user(), 'pages.editor'); //control del permisos
    }


    public function index(Request $request)
    {
        
        $AccessControl = $this->AccessControl;
        $AccessControl->IfCanNot('view')->redirect();

        $menu = Menu::getMenu(Auth::user());

        $section_name = $this->section_name;

        //$pages = PageMaker::orderBy('id', 'desc')->paginate(20);
        $pages = PageMaker::search($request->get('search'))
                    ->active($request->get('active'))
                    ->lang($request->get('lang'))
                    ->orderBy('id', 'desc')
                    ->paginate(20);

        $params = compact('AccessControl', 'menu','section_name', 'pages');

        return view('panel.pages.index', $params);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $AccessControl = $this->AccessControl;
        $AccessControl->IfCanNot('create')->redirect();

        $menu = Menu::getMenu(Auth::user());

        $section_name = $this->section_name;

        $page = new PageMaker();

        $params = compact('AccessControl', 'menu','section_name', 'page');

        return view('panel.pages.create', $params);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $AccessControl = $this->AccessControl;
        $AccessControl->IfCanNot('create')->redirect();
        
        $section_name = $this->section_name;

        $errors = $this->validate($request, [
            'name' => 'required|min:5',
            'lang' => 'required',
            'title' => 'required|min:5',
            'url' => 'required|min:5|unique:pages',
            'keywords' => '',
            'description' => '',
            'content' => '',
            'active' => '',
        ]);

        $page = new PageMaker();

        if (!$request->has('active')){
            $request->request->add(['active' => '0']);
        }

        $page->fill($request->all());
        $page->save();

        return \Redirect::action('Panel\PagesController@edit', $page->id);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        $AccessControl = $this->AccessControl;
        $AccessControl->IfCanNot('edit')->redirect();

        $menu = Menu::getMenu(Auth::user());

        $section_name = $this->section_name;

        $page = PageMaker::findOrFail($id);

        $params = compact('AccessControl', 'menu','section_name', 'page');

        return view('panel.pages.edit', $params);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {

        $AccessControl = $this->AccessControl;
        $AccessControl->IfCanNot('create')->redirect();

        $section_name = $this->section_name;

        $errors = $this->validate($request, [
            'name' => 'required|min:5',
            'lang' => 'required',
            'title' => 'required|min:5',
            'url' => 'required|min:5',
        ]);

        $page = PageMaker::findOrFail($request->id);

        $pageExist = $page->where('url', '=', $request->url)
                    ->where('id', '!=',  $request->id)
                    ->first();

        if (!$request->has('active')){
            $request->request->add(['active' => '0']);
        }


        if(empty($pageExist->url)){
            $page->fill($request->all());
            $page->save();
        }else{
            \Session::flash('message-warning', 'No se puede actualizar la página por que la URL :  "' . $pageExist->url . '"" ya existe');
            return redirect()->back();
        }

        $page->fill($request->all());
        $page->save();

        return \Redirect::action('Panel\PagesController@edit', $page->id);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

        $AccessControl = $this->AccessControl;
        $AccessControl->IfCanNot('delete')->redirect();


        $page = PageMaker::find($id);

        if(!empty($page->id)){
            PageMaker::destroy($id);
            Session::flash('message-warning', 'El registro (' . $page->id . '/' . $page->name . ') fué eliminado');
        }else{
            Session::flash('message-warning', 'El registro no puede borrarse por no existe');
        }

        return redirect(config('panel.routes.pages.index'));

    }

    public function load_tpl(){

        $template   = \Input::get('template');
        $path       = config('panel.pages.path') . $template;

        if(file_exists($path)){
            return file_get_contents($path);
        }else{
            return "Template not found: <br />" . $path . " in " . getcwd();
        }
    }
}
