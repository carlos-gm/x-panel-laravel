<?php

namespace App\Http\Controllers\Panel;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Auth;

use App\Panel\User;
use App\Panel\Group;
use App\Panel\Menu;
use App\Panel\AccessControl;

class UsersController extends Controller
{

    public $section_name = "Gestión de usuarios";
    public $AccessControl;

    public function __construct(){

        $this->AccessControl = new AccessControl(Auth::user(), 'config.users'); //control del permisos

    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index(Request $request)
    {

        $AccessControl = $this->AccessControl;
        $AccessControl->IfCanNot('view')->redirect();

        $menu = Menu::getMenu(Auth::user());

        $section_name = $this->section_name;

        //$users = \DB::table('users')->orderBy('id','asc')->paginate(15);
        //$user = new User;
        //$users = $user->orderBy('id','desc')->paginate(15);
        
        $users = User::search($request->get('search'))
                    ->active($request->get('active'))
                    ->orderBy('id', 'desc')
                    ->paginate(20);

        $params = compact('AccessControl', 'menu','section_name','users');

        return view('panel.users.index', $params);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {  

        $AccessControl = $this->AccessControl;
        $AccessControl->IfCanNot('create')->redirect();

        $menu = Menu::getMenu(Auth::user());

        $section_name = $this->section_name;

        $group_list = Group::lists('name', 'id');

        $params = compact('menu' , 'section_name', 'group_list');

        return view('panel.users.create', $params);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request  $request
     * @return Response
     */
    public function store(Request $request)
    {

        $AccessControl = $this->AccessControl;
        $AccessControl->IfCanNot('create')->redirect();

        $section_name = $this->section_name;

        $errors = $this->validate($request, [
            'name' => 'required|min:3',
            'email' => 'required|min:8|email|unique:users',
            'password' => 'required|min:5'
        ]);

        $user = new User();

        if (!$request->has('active')){
            $request->request->add(['active' => '0']);
        }

        $user->fill($request->all());
        $user->save();

        return \Redirect::action('Panel\UsersController@edit', $user->id);
     
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {

        $AccessControl = $this->AccessControl;
        $AccessControl->IfCanNot('edit')->redirect();

        $section_name = $this->section_name;

        $menu = Menu::getMenu(Auth::user());

        $db_users = new User;

        $user = $db_users->find($id);

        $group_list = Group::lists('name', 'id');

        $params = compact('menu', 'section_name', 'user', 'group_list');

        //return view('panel.assets.wellcome', $params);
        return view('panel.users.edit', $params);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request  $request
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request)
    {


        $AccessControl = $this->AccessControl;
        $AccessControl->IfCanNot('edit')->redirect();

        $errors = $this->validate($request, [
            'name' => 'required|min:3',
            'email' => 'required|min:8|email',
        ]);

        $user = User::findOrFail($request->id);

        if (!$request->has('active')){
            $request->request->add(['active' => '0']);
        }

        /* si el usuario cambia de mail y ya existe */
        $dbUser = $user->where('email', '=', $request->email)
                    ->where('id', '!=',  $request->id)
                    ->first();

        if(empty($dbUser->email)){
            $user->fill($request->all());
            $user->save();
        }else{
            Session::flash('message-warning', 'No se puede actualizar el usuario por que el E-mail:  ' . $dbUser->email . ' ya existe');
            return redirect()->back();
        }

        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {

        $AccessControl = $this->AccessControl;
        $AccessControl->IfCanNot('delete')->redirect();

        try
        {
            $user = User::findOrFail($id);
            User::destroy($user->id);
            Session::flash('message-warning', ' - El registro (' . $user->id . ') para el e-mail ' . $user->email . " fué eliminado");
        }catch(ModelNotFoundException $e){
            Session::flash('message-warning', ' - Este usurio ya no existe');
        }

        return redirect(config('panel.routes.users.index'));

    }

}
