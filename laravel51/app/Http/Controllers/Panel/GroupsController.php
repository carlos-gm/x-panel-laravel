<?php

namespace App\Http\Controllers\Panel;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Auth;
use Redirect;

use App\Panel\Group;
use App\Panel\Menu;
use App\Panel\AccessControl;

class GroupsController extends Controller
{

    public $section_name = "Grupos del sistema de usuarios";
    public $AccessControl;

    public function __construct(){

        $this->AccessControl = new AccessControl(Auth::user(), "config.groups");

    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        $AccessControl = $this->AccessControl;
        $AccessControl->IfCanNot('view')->redirect();

        $menu = Menu::getMenu(Auth::user());

        $section_name = $this->section_name;

        $groups = Group::orderBy('id', 'desc')->paginate(20);

        $params = compact('AccessControl', 'menu','section_name','groups');

        return view('panel.groups.index', $params);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        $AccessControl = $this->AccessControl;
        $AccessControl->IfCanNot('create')->redirect();

        $menu = Menu::getMenu(Auth::user());

        $section_name = $this->section_name;

        $permissions = config('panel.permissions');

        $group = (object)Array("permissions" => Array()); //Se da un formato de objecto a un array vacio para rellenar

        $params = compact('AccessControl', 'menu' , 'section_name', 'group', 'permissions');

        return view('panel.groups.create', $params);

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $AccessControl = $this->AccessControl;
        $AccessControl->IfCanNot('create')->redirect();

        $menu = Menu::getMenu(Auth::user());
        
        $section_name = $this->section_name;

        $errors = $this->validate($request, [
            'name' => 'required|min:3|unique:groups',
            'description' => 'required|min:5'
        ]);

        $group = new Group();

        $JsonPermissions = $group->parsePermissions();

        if (!$request->has('active')){
            $request->request->add(['active' => '0']);
        }

        $group->fill($request->all());
        $group->permissions = $JsonPermissions;
        $group->save();

         return Redirect::action('Panel\GroupsController@edit', $group->id);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        $AccessControl = $this->AccessControl;
        $AccessControl->IfCanNot('edit')->redirect();

        $section_name = $this->section_name;

        $menu = Menu::getMenu(Auth::user());

        $Group = new Group;

        $group = $Group->find($id);

        $group->permissions = (array)json_decode($group->permissions);

        $permissions = config('panel.permissions');

        $params = compact('AccessControl', 'menu', 'section_name', 'group', 'permissions');

        return view('panel.groups.edit', $params);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    
    public function update(Request $request, $id)
    {

        $AccessControl = $this->AccessControl;
        $AccessControl->IfCanNot('edit')->redirect();

        $Group = new Group;

        $JsonPermissions = $Group->parsePermissions();
        
        $group = Group::findOrFail($request->id);

        if (!$request->has('active')){
            $request->replace(array('active' => '0'));
        }

        $group->fill($request->all());
        $group->permissions = $JsonPermissions;
        
        $group->save();

        Session::flash('message', 'Campos actualizados correctamente');

        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
