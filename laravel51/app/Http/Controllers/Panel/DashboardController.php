<?php

namespace App\Http\Controllers\Panel;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Redirect;

use App\Panel\User;
use App\Panel\Menu;
use Auth;

class DashboardController extends Controller
{
    
    public $section_name = "Dashboard";

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    
    public function __construct(){


    }

    public function index()
    {

        $menu = Menu::getMenu(Auth::user());

        $section_name = $this->section_name;

        $params = compact('menu','section_name');

        return view('panel.main.index', $params);

    }


}
