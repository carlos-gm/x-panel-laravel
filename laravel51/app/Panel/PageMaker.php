<?php

namespace App\Panel;

use Illuminate\Database\Eloquent\Model;

class PageMaker extends Model
{

    protected $table = 'pages';
	
	protected $fillable = ['name', 'lang', 'title', 'url', 'keywords', 'description', 'content', 'active'];

	protected $hidden = ['remember_token'];


    public function scopeSearch($query, $search)
    {
        if(!empty(trim($search))){

            $query->where('name', 'LIKE', "%$search%")
                    ->orWhere('title', 'LIKE', "%$search%");
        }

    }

    public function scopeLang($query, $lang){

		if(!empty($lang)){
			$query->where('lang', '=', $lang);
		}

    }

    public function scopeActive($query, $active){

        //se hace un apaño, ya que no entiende que active sea "cero"
        if($active == 2){
            $query->where('active', '=', "0");
        }elseif($active == 1){
            $query->where('active', '=', "1");
        }

    }

}
