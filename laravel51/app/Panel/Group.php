<?php

namespace App\Panel;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Foundation\Auth\Access\Authorizable;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;

class Group extends Model implements AuthenticatableContract,
                                    AuthorizableContract,
                                    CanResetPasswordContract
{
    use Authenticatable, Authorizable, CanResetPassword;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'groups';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'description', 'permissions', 'active' ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    
    protected $hidden = [];

    public function parsePermissions()
    {

        $permissions = \Input::get("permissions");

        $ArrPermissions = Array();

        if(is_array($permissions)){

            foreach($permissions as $key => $appValues){

                $ArrPermissions[$key] = $appValues;

                foreach($appValues as $type => $val){
                    
                    foreach($val as $kval => $aux){
                        $ArrPermissions[$key][$type][$kval] = true;
                    }
                    //$ArrPermissions[$key][$type][$val][] = true;
                }

            }

            $JsonPermissions = json_encode($ArrPermissions);
        }else{
            $JsonPermissions = json_encode(Array()); //TODO funciona pero no es elegante :P
        }

        return $JsonPermissions;
    }

    public function getGroupByUser($user){

        $group =  \DB::table('groups')->where('id', '=', $user->group_id)->first();
        $permissions = json_decode($group->permissions);

        return $group->permissions;

    }
   
}
