<?php

namespace App\Panel;

use Illuminate\Database\Eloquent\Model;
use App\Panel\Group;

class Menu extends Model{

	static function getMenu($User)
	{
		$menu 	= config('panel.permissions');
		return self::getHtmlMenu($User, $menu);
	}

	static function getHtmlMenu($user, $arr_menu)
	{

		$Group = new Group;

		$permissions = $Group->getGroupByUser($user);
		$permissions = (array)json_decode($permissions);

		$html = "";	
		$menu = "";
		$sub_menu = "";


		foreach($arr_menu as $key => $group){
			

			//menu activo -> class="active open"
			//dd($key);
			//dd($permissions);
			//dd($permissions[$key]);
			//

			if(!empty($permissions[$key])){

				$menu.= '
					<li class="start {' . $key . '}">
						<a href="javascript:;">
						<i class="' . $group['icon'] . '"></i>
						<span class="title">' . $group['name'] . '</span>
						<span class="arrow {#' . $key . '#}"></span>
						</a>';

				$apps = $group['apps'];

				if(is_array($apps)){

					$sub_menu.= '<ul class="sub-menu">';

					foreach($apps as $klink => $link){

						//menu activo -> class="active"

						if(!empty($permissions[$key]->$klink)){

							$url = self::getUrl($link['route']);

							$path = \Request::segment(2);
							
							if($path == $klink){

								$active = "active";

								$menu = str_replace('{' . $key . '}', 'active', $menu); //remplazamos la marca para que el menú se despliegue
								$menu = str_replace('{#' . $key . '#}', 'open', $menu); //remplazamos la marca para que el menú muestre la flecha bien

							}elseif(($key == "dashboard") && (empty($path))){ // caso especial para dashboard 

								$active = "active";

								$menu = str_replace('{dashboard}', 'active', $menu); //remplazamos la marca para que el menú se despliegue
								$menu = str_replace('{#dashboard#}', 'open', $menu); //remplazamos la marca para que el menú muestre la flecha bien

							}else{

								$active = "";
							}

							$app = false;

							if(isset($permissions[$key]->$klink->view)){

								$sub_menu.= '
										<li class="'.$active.'">
											<a href="' . $url . '">
											<i class="' . $link['icon'] . '"></i>
											' . $link['name'] . '</a>
										</li>';

								$active = "";
								
								$app = true;

							}else{

							}
						}else{

						}
					}

						$sub_menu.= '</ul>';
				}

				if($app == true){
					$menu = $menu . $sub_menu;
					$sub_menu = "";
				}else{
					$menu = "";
				}

				$html.= $menu.="</li>";
				$app = false;

				$submenu 	= "";
				$app 		= "";

			}
		}

		return $menu;

	}

	static function getUrl($route){
		return "/". str_replace(".", "/", $route);
	}


}