<?php

namespace App\Panel;

use Auth;
use App\Panel\Group;
use Illuminate\Database\Eloquent\Model;

class AccessControl extends Model
{
    
    public $UserPermissions;
    public $permissions;
    public $zone;
    public $tmp_zone;
    public $canView, $canCreate, $canEdit, $canUpdate, $canActivate;
    public $redirectUrl = "/panel?error=1";
    public $redirect;
    public $dbg = 1;

    public function __construct($User, $zone){

    	$Group = new Group;

    	$this->UserPermissions = $Group->find($User->group_id);
    	$this->permissions = json_decode($this->UserPermissions->permissions);
    	
    	$this->in($zone);
    	
    }

    public function in($tmp_zone)
    {
    	$this->tmp_zone = $tmp_zone;

    	$zone = str_replace(".","->", $tmp_zone);
    	$this->zone = $zone;

    	$this->canView 		= $this->can("view");

    	$this->canCreate 	= $this->can("create");
    	$this->canEdit 		= $this->can("edit");
    	$this->canUpdate 	= $this->can("update");
    	$this->canDelete 	= $this->can("delete");
    	$this->canActivate 	= $this->can("activate");

/*
		if($this->canView == true){
			$this->canNotView = false;
		}else{
			$this->canNotView = true;
			\Session::flash('message-warning', 'No tienes permisoss');
			$this->redirect();
		}
*/  
    	return $this;
    }

    public function myEval($string){

    	@eval($string);

    	if($can == true){
    		return true;
    	}else{
    		return false;
    	}
    }

    public function can($option)
    {
    	$string = '$can = $this->permissions->' . $this->zone . '->' . $option . ';';
    	
    	return $this->myEval($string);
    }

    public function IfCanNot($verb)
    {

    	switch($verb){
    		case "view":
    			$this->redirect = ($this->canView == true) ? false : true;
    			break;
    		case "edit":
    			$this->redirect = ($this->canEdit == true) ? false : true;
    			break;
            case "create":
                $this->redirect = ($this->canCreate == true) ? false : true;
                break;
    		case "update":
    			$this->redirect = ($this->canUpdate == true) ? false : true;
    			break;
    		case "delete":
    			$this->redirect = ($this->canDelete == true) ? false : true;
    			break;
    	}

    	return $this;
    }

    public function redirect($url = "")
    {

    	if($this->redirect == true){

    		if(!empty($url)){
    			header("Location: " . $url);
    		}else{
		    	header("Location: " . $this->redirectUrl);
		    }
		    
            die();
	    }
    }

}
