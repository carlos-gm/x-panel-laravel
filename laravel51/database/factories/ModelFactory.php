<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

$factory->define(App\Panel\User::class, function ($faker) {
    return [
        'group_id' => $faker->randomElement([1,3]),
        'name' => $faker->name,
        'email' => $faker->email,
        'password' => bcrypt(str_random(10)),
        'active' => $faker->randomElement([0,1]),
        'remember_token' => str_random(10),
    ];
});


$factory->define(App\Panel\Group::class, function ($faker) {
    return [
        'name' => $faker->name,
        'description' => $faker->text,
        'permissions' => $faker->text,
        'active' => $faker->randomElement([0,1]),
    ];
});