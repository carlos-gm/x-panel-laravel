<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::dropIfExists('pages');

        Schema::create('pages', function (Blueprint $table) {

            $table->increments('id');
            $table->string('name', 255);
            $table->string('lang', 2);
            $table->string('title', 255);
            $table->string('url')->unique();
            $table->string('keywords', 255);
            $table->text('description');
            $table->longText('content');
            $table->boolean('active')->default(false);
            $table->rememberToken();
            $table->timestamps();
            
        });

        DB::statement('ALTER TABLE pages ADD FULLTEXT search(content)');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

        Schema::table('pages', function($table) {
            $table->dropIndex('search');
        });

        Schema::drop('pages');
    }
}
