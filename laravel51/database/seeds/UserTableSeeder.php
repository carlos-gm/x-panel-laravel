<?php

use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        /*creamos usuario administrador manualmente*/
        factory(App\Panel\User::class)->create([
            'group_id' => '4', //Super Admin
            'name' => 'Carlos',
            'email' => 'carlosgarciamacias@gmail.com',
            'password' => '12345',
            'active' => 1
        ]);

        factory(App\Panel\User::class)->create([
            'group_id' => '1', //Invitado
            'name' => 'Demo',
            'email' => 'demo@gmail.com',
            'password' => '12345',
            'active' => 1
        ]);

        factory(App\Panel\User::class, 50)->create();

    }
}
