<?php

use Illuminate\Database\Seeder;

class GroupTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        /*creamos grupo administrador manualmente*/

        factory(App\Panel\Group::class)->create([
            'id'            => 1,
            'name'          => 'Invitados',
            'description'   => 'Grupo Invitados',
            'permissions'    => '',
            'active'        => true,
        ]);

        factory(App\Panel\Group::class)->create([
            'id'            => 2,
            'name'          => 'Editores',
            'description'   => 'Grupo Editores',
            'permissions'    => '',
            'active'        => true,
        ]);

        factory(App\Panel\Group::class)->create([
            'id'            => 3,
            'name'          => 'Administradores',
            'description'   => 'Grupo administradores',
            'permissions'    => '',
            'active'        => true,
        ]);

        factory(App\Panel\Group::class)->create([
            'id'            => 4,
            'name'          => 'Super Admin',
            'description'   => 'Grupo Super Admin',
            'permissions'    => '{"dashboard":{"main":{"view":true}},"config":{"users":{"view":true,"edit":true,"create":true,"update":true,"delete":true,"activate":true},"groups":{"view":true,"edit":true,"create":true,"update":true,"activate":true},"snipets":{"view":true}}}',
            'active'        => true,
        ]);

    }
}
